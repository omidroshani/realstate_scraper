from setuptools import setup, find_packages

install_requires = [
    "Scrapy==2.6.1",
    "scrapy-splash==0.8.0",
    "requests==2.27.1",
    "fake-useragent==0.1.11",
    "pytz==2021.3",
    "geocoder==1.38.1",
    "python-dotenv==0.20.0",
    "spidermon==1.17.1",
    "schematics==2.1.1",
    "pyopenssl==22.0.0",
    "cryptography==38.0.4"
]

setup(
    name='realestate',
    packages=find_packages(),
    version='0.1.0',
    description='',
    author='',
    license='',
    install_requires=install_requires,
)
