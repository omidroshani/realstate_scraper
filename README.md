### Stekkies Assessment

Thanks for taking our assessment. All information is listed below to get you started! This Scrapy project's aim is to scrape new housing listings from real estate websites. You'll work on fixing one of them and creating a new one.

## How to get started

1. Create your new conda environment by running `conda env create -f environment.yml` from root directory.
2. Activate your new `stekkies` conda environment by running `conda activate stekkies`.
3. Install all python dependencies necessary for this project by running `pip install -e .` from root directory.

## General understanding of the Scrapy repo structure

1. Look at the file `spiders/spider.py` to check our template spider: `TemplateSpider`. We would like you to stick as much as possible to the coding style we've made.
2. There are two spider classes below where you will work on (but keep on reading for now). Note that all spiders are inherited from `BaseStekkiesSpider`, take a moment to look at it.
3. In `main.py` you'll see the spiders that will be run by executing the following `python realestate/main.py` from the root directory.
4. Results of your scrape will be stored in `scrape_results/items.json`, where you can check the scraped items per spider.
5. We have a number of monitors and item validators in place (see `monitors.py` and `validators.py`) to test the outcomes of our Spiders. Make sure when finishing your work that it passes all tests.

## Let's get to work!

1. Head back to `spiders/spider.py` and start working on the spider `CarlavandenBrink`. Currently the spider is not working, because the website has changed their structure.
Find out what the current bugs are and solve them so that the monitors and validators pass the tests.
2. Further down in `spiders/spider.py` you'll see the `PrincipleProperties` class. This is a website which we don't cover yet. Make sure to finish the code in order to scraper the housing listings from this website.
Once again, make sure by the end that it passes all the tests of monitors and validators. Don't forget to uncomment the `process.crawl(...)` line for this spider in `main.py`.

## Guidelines per scraper

Underneath we have a checklist that you can go through after finishing every scraper

- Does the scraper contain only CSS selectors? (sometimes it is easier to use XPATH, but we prefer CSS selectors)
- Does the scraper sort in descending date (newest first)?
- Does the scraper scrape a limited amount of the newest listings? (old listing are irrelevant, the first 30 listings is more than enough)
- Does the scraper include a check to skip over listings that are already unavailable (rented out, under option, etc.)
- Does the scraper only except a listing when it fails data parsing? It should be skipped (via a continue statement) if listing is not a proper house (i.e. garage box or only rooms instead of apartments) or has missing information.
- Does the output fields hold the preferred data type (integer, url, text) that's stated in the validator?