# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class HouseItem(scrapy.Item):
    url = scrapy.Field()
    address = scrapy.Field()
    country = scrapy.Field()
    city = scrapy.Field()
    neighbourhood = scrapy.Field()
    postal_code = scrapy.Field()
    price = scrapy.Field()
    rooms = scrapy.Field()
    bedrooms = scrapy.Field()
    surface = scrapy.Field()
    photo = scrapy.Field()
    website = scrapy.Field()
    description = scrapy.Field()
    scraped_at = scrapy.Field()
    balcony = scrapy.Field()
    garden = scrapy.Field()
    bath = scrapy.Field()
    furnished = scrapy.Field()
    sharing = scrapy.Field()
    pet_friendly = scrapy.Field()
    senior_home = scrapy.Field()
    single_room = scrapy.Field()
    income_requirement = scrapy.Field()
    students = scrapy.Field()
    location_query = scrapy.Field()
    realtor = scrapy.Field()
    realtor_link = scrapy.Field()
    location_identifier = scrapy.Field()

class SaleHouseItem(scrapy.Item):
    url = scrapy.Field()
    address = scrapy.Field()
    city = scrapy.Field()
    postal_code = scrapy.Field()
    price = scrapy.Field()
    rooms = scrapy.Field()
    bedrooms = scrapy.Field()
    surface = scrapy.Field()
    plot = scrapy.Field()
    photo = scrapy.Field()
    description = scrapy.Field()
    website = scrapy.Field()
    realtor = scrapy.Field()
    realtor_link = scrapy.Field()
    miscellaneous = scrapy.Field()
    scraped_at = scrapy.Field()
    location_identifier = scrapy.Field()

class FundaRealEstate(scrapy.Item):
    agent = scrapy.Field()
    url = scrapy.Field()

