from schematics.models import Model
from schematics.types import URLType, DateTimeType, BooleanType, NumberType, BaseType
from schematics.exceptions import ValidationError

class IntType(BaseType):
    MESSAGES = {'IntType': 'Value is not of type int'}

    def validate_int(self, value):
        if not isinstance(value, int):
            raise ValidationError(self.messages['IntType'])

class StringType(BaseType):
    MESSAGES = {'StringType': 'Value is empty string'}

    def validate_string(self, value):
        if not isinstance(value, str) or value == '':
            raise ValidationError(self.messages['StringType'])


class HouseItemNL(Model):
    url = URLType(required=True)
    address = StringType(required=True)
    country = StringType(required=True)
    city = StringType()
    neighbourhood = StringType()
    postal_code = IntType()
    price = IntType(required=True)
    rooms = IntType()
    bedrooms = IntType()
    surface = IntType(required=True)
    photo = URLType(serialize_when_none=True, default="https://stekkies.com/static/roomraider/img/no-image-found.jpg")
    website = StringType(required=True)
    description = StringType()
    scraped_at = DateTimeType(required=True)
    balcony = BooleanType()
    garden = BooleanType()
    bath = BooleanType()
    furnished = BooleanType()
    sharing = BooleanType()
    pet_friendly = BooleanType()
    senior_home = BooleanType()
    single_room = BooleanType()
    income_requirement = BooleanType()
    students = BooleanType()
    location_query = StringType()
    realtor = StringType()
    realtor_link = StringType()
    location_identifier = StringType()

    def validate_rooms(self, data, value):
        if "bedrooms" not in data and "rooms" not in data:
            raise ValidationError("Either 'bedrooms' or 'rooms' attribute should be filled.")