# -*- coding: utf-8 -*-
import os
import logging
from dotenv import load_dotenv

from realestate.user_agents import user_agents

# Scrapy settings for roomraider project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

# Crawl responsibly by identifying yourself (and your website) on the user-agent

logger = logging.getLogger(__name__)

# Environment variables
load_dotenv()
SCRAPING_CITIES = {'Amsterdam': {'latitude': '52.370216', 'longitude': '4.895168'},
                   'Rotterdam': {'latitude': '51.9244201', 'longitude': '4.4777325'},
                   'Den Haag': {'latitude': '52.079467', 'longitude': '4.312487'},
                   'Utrecht': {'latitude': '52.090736', 'longitude': '5.121420'},
                   'Groningen': {'latitude': '53.219383', 'longitude': '6.566502'},
                   'Eindhoven': {'latitude': '51.439372', 'longitude': '5.477503'},
                   'Haarlem': {'latitude': '52.380882', 'longitude': '4.636848'},
                   'London': {'latitude': '51.507351', 'longitude': '-0.127758'}}

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
# Change according to scraping Bee payment plan
#CONCURRENT_REQUESTS = 32

# Disable cookies (enabled by default)
COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
TELNETCONSOLE_ENABLED = False

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
SPLASH_URL = 'http://localhost:8050'

DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': None,
    'realestate.middlewares.RotateUserAgentMiddleware': 110,
    'scrapy.downloadermiddlewares.retry.RetryMiddleware': 100,
    'scrapy_splash.SplashCookiesMiddleware': 723,
    'scrapy_splash.SplashMiddleware': 725,
    'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
}

RETRY_ENABLED = True
RETRY_TIMES = 3

SPIDER_MIDDLEWARES = {
    'scrapy.spidermiddlewares.referer.RefererMiddleware': 100,
}

USER_AGENT_CHOICES = user_agents

# Logging
LOG_ENABLED = True
LOG_LEVEL = 'DEBUG'
LOG_FORMAT = '%(asctime)s %(levelname)s %(module)s: %(message)s'

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
EXTENSIONS = {
    # 'scrapy.extensions.telnet.TelnetConsole': None,
    'spidermon.contrib.scrapy.extensions.Spidermon': 500,
    'realestate.extensions.ItemCollectorExtension': 499
}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'spidermon.contrib.scrapy.pipelines.ItemValidationPipeline': 800,
}

# SPIDERMON
SPIDERMON_ENABLED = True

SPIDERMON_SPIDER_OPEN_MONITORS = (
    # list of monitor suites to be executed when the spider starts
)

SPIDERMON_SPIDER_CLOSE_MONITORS = (
    'realestate.monitors.SpiderCloseMonitorSuite',
)

SPIDERMON_VALIDATION_ADD_ERRORS_TO_ITEMS = False
SPIDERMON_VALIDATION_DROP_ITEMS_WITH_ERRORS = False

SPIDERMON_UNWANTED_HTTP_CODES = [400, 407, 429, 500, 502, 503, 504, 523, 540, 541]
SPIDERMON_UNWANTED_HTTP_CODES_MAX_COUNT = 5

SPIDERMON_EXPECTED_FINISH_REASONS = ["finished"]

# Spidermon Slack settings
SPIDERMON_SLACK_FAKE = True
