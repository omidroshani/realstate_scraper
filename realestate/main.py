from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings
from realestate.spiders import spider
import scrapy.signals
import datetime as dt
import logging
import json
import os

logger = logging.getLogger(__name__)

def scraper_results(headers: dict, url: str):
    logger.info('Start')

    # Create list where dicts will be appended from House items
    scrape_results = []

    # Log time of scrape run
    scraper_run = dt.datetime.now().replace(second=0, microsecond=0).strftime('%Y-%m-%d %H-%M-%S')

    def crawler_results(item, response, spider):
        """ Function that appends all items from every spider"""
        scrape_results.append(dict(item))

    # Configuring Logging
    configure_logging({'LOG_FORMAT': '%(levelname)s: %(message)s'})

    # Retrieve settings from project
    runner = CrawlerRunner(get_project_settings())

    # Get information on settings of cities we support
    cities = runner.settings.attributes['SCRAPING_CITIES'].value

    # General spiders for multiple cities, where the cities argument contains all city information
    runner.crawl(spider.PrincipleProperties, recent_houses_urls=[], headers=headers, scraper_run=scraper_run, url=url, cities=cities, country="Netherlands")

    # Spiders for Amsterdam area
    amsterdam = {k:v for k, v in cities.items() if k == 'Amsterdam'}
    runner.crawl(spider.CarlavandenBrink, recent_houses_urls=[], headers=headers, scraper_run=scraper_run, url=url, cities=amsterdam, country="Netherlands")

    for p in runner.crawlers:
        p.signals.connect(crawler_results, signal=scrapy.signals.item_scraped)
    d = runner.join() # return a Deferred that will fire once all crawling jobs have completed
    d.addBoth(lambda _: reactor.stop())

    # Start the reactor (i.e., the event loop manager that drives the event loop and thus the spiders) manually
    reactor.run()

    # If folder realestate_scraper/scrape_results doesn't exist yet, create one, After reactor has stopped
    if not os.path.isdir('scrape_results'):
        os.mkdir('scrape_results')

    # Open a .json file and append all scraped listings
    with open('scrape_results/items.json', 'w') as f:
        json.dump(scrape_results, f, default=_date_json_encoder, indent=4)

# Function to encode python datetime objects to convert into the .json file
def _date_json_encoder(value: dt.datetime) -> str:
    if isinstance(value, (dt.date, dt.datetime)):
        return value.isoformat()

if __name__ == '__main__':
    # Initiate the main function
    scraper_results(headers={'Content-type': 'application/json', 'User-Agent': 'tester'},
                    url='https://stekkies.com')