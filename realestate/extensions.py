import logging
from scrapy import signals
from scrapy.utils.project import get_project_settings
from scrapy.utils.serialize import ScrapyJSONEncoder

logger = logging.getLogger(__name__)

class ItemCollectorExtension:
    def __init__(self, stats):
        self.settings = get_project_settings()
        self.stekkies_url = self.settings['STEKKIES_URL']
        self.stekkies_request_headers = self.settings['STEKKIES_SERVER_REQUEST_HEADERS']
        self.stats = stats
        self.json_encoder = ScrapyJSONEncoder()
        self.items = []


    @classmethod
    def from_crawler(cls, crawler):
        spider = cls(crawler.stats)
        crawler.signals.connect(spider.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(spider.add_item, signal=signals.item_scraped)
        return spider

    def spider_opened(self, spider):
        logger.info('{spider} Spider Opened'.format(spider=spider.name))
        self._set_custom_settings(spider)
        self.stats.set_value('items_scrapeable', 0, spider=spider)
        self.stats.set_value('items_already_in_db', 0, spider=spider)
        self.stats.set_value('items_available_for_rent', 0, spider=spider)
        self.stats.set_value('scraper_exceptions/errors', 0, spider=spider)
        self.stats.set_value('location_identifier/errors', 0, spider=spider)
        self.stats.set_value('listings_present_on_page', True, spider=spider)

    def _set_custom_settings(self, spider):
        if spider.country == "Netherlands" and spider.listing_type == "house":
            self.settings.set("SPIDERMON_VALIDATION_MODELS", ["scraper.validators.HouseItemNL"])
        elif spider.country == "Portugal" and spider.listing_type == "house":
            self.settings.set("SPIDERMON_VALIDATION_MODELS", ["scraper.validators.HouseItemPT"])
        elif spider.country == "Portugal" and spider.listing_type == "single_room":
            self.settings.set("SPIDERMON_VALIDATION_MODELS", ["scraper.validators.RoomItemPT"])

    def add_item(self, item):
        self.items.append(item)

