import re
import datetime as dt
import logging
import json

import scrapy

from realestate.items import HouseItem
from realestate.spiders.base_spider import BaseStekkiesSpider
from scrapy.exceptions import CloseSpider



logger = logging.getLogger(__name__)

# Note that this is a template class you can use to check out our regular spider standards.
class TemplateSpider(BaseStekkiesSpider):
    name = 'website_name'

    base_url = 'https://www.website.nl'

    def start_requests(self):
        pages = range(1, 3)
        start_urls = [f'{self.base_url}/woningaanbod/huur?page={page}' for page in pages]

        for start_url in start_urls:
            yield scrapy.Request(start_url, callback=self.parse)


    def parse(self, response):
        listings = response.css('.selector')

        for listing in listings:
            try:
                self.crawler.stats.inc_value('items_scrapeable')
                status = listing.css('.selector::text').get()
                if status and ('Verhuurd' in status or 'Onder optie' in status):
                    continue
                self.crawler.stats.inc_value('items_available_for_rent')
                item = HouseItem()
                item['url'] = self.base_url + listing.css('.selector::attr(href)').get()
                if not item['url'] in self.recent_houses_urls:
                    self.crawler.stats.inc_value('items_already_in_db')
                    continue
                item['photo'] = listing.css('.selector::attr(src)').get()
                item['country'] = self.country
                item['address'] = listing.css('.selector::text').get()
                item['city'] = listing.css('.selector::text').get()
                postal_code = listing.css('.selector::text').re_first(r'([0-9]{4})')
                item['postal_code'] = int(postal_code)
                price = listing.css('.selector::text').get()
                item['price'] = int(re.sub(r'\D', '', price))
                surface = listing.css('.selector::text').get()
                item['surface'] = int(re.sub(r'\D', '', surface))
                rooms = listing.xpath(".//span[contains(text(),'Woonoppervlakte')]/following-sibling::span[1]/text()").get()
                item['rooms'] = int(re.sub(r'\D', '', rooms))
                item['website'] = self.name
                item['scraped_at'] = self.scraped_at
                yield scrapy.Request(item['url'], meta={'item': item}, callback=self.parse_inner)

            except Exception as e:
                self.handle_exception()

    def parse_inner(self, response):
        try:
            item = response.meta['item']
            description = " ".join(response.css('.selector > p::text').getall()).strip()
            item = self.read_description(item, description)
            yield item

        except Exception as e:
            self.handle_exception()


"""
Start working below to fix an existing spider and create a new one
"""

# There are some bugs in this spider, please make sure to fix them to let it pass the validator
class CarlavandenBrink(BaseStekkiesSpider):
    name = "Carla van den Brink"
    start_urls = [
        'https://www.vandenbrink.nl/nl/realtime-listings/consumer'
    ]

    def parse_inner(self, response):
        # Description of each item in this spider is not well defined so it is better to ignore
        try:
            item = response.meta['item']
            item = self.read_description(item, item['description'])
            yield item
        except:
            self.handle_exception()

    def parse(self, response):
        data = json.loads(response.body)
        data = [i for i in data if i["status"] == "Beschikbaar" and i["isRentals"] == True]
        data.sort(key=lambda x: x["added"], reverse=True)

        for house in data:
            try:
                self.crawler.stats.inc_value('items_scrapeable')
                self.crawler.stats.inc_value('items_available_for_rent')
                item = HouseItem()
                item["url"] = "https://www.vandenbrink.nl" + house["url"]
                if item['url'] in self.recent_houses_urls:
                    self.crawler.stats.inc_value('items_already_in_db')
                    continue
                item["address"] = house["address"]
                item["city"] = house["city"]
                item['country'] = self.country
                item["postal_code"] = int(house["zipcode"].split()[0])
                item["price"] = house["rentalsPrice"]
                item["rooms"] = int(house["rooms"])
                item["surface"] = int(house["livingSurface"])
                item["photo"] = house["photo"]
                item["website"] = self.name
                item['scraped_at'] = self.scraped_at
                item['furnished'] = house['isFurnished']
                item['balcony'] = True if isinstance(house['balcony'], str) else False
                item['garden'] = True if isinstance(house['garden'], str) else False
                item['description'] = house['description']
                yield scrapy.Request(item["url"], meta={"item": item}, callback=self.parse_inner)
            except Exception as e:
                self.handle_exception()

# Finish creating a new spider for www.principleproperties.nl
class PrincipleProperties(BaseStekkiesSpider):
    name = 'Principle Properties'

    def start_requests(self):
        start_urls = ["https://www.principleproperties.nl/en/rental-apartments/?filter-sort-by=published&filter-sort-order=desc"]

        for url in start_urls:
            yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):

        listings = response.css('.content')
        first_listing = listings[0]
        articles = first_listing.css('article')
        
        for article in articles:
            try:
                item = HouseItem()
                item['url'] = article.css('a::attr(href)').get()
                item['photo'] = article.css('a').css('img::attr(src)').get()
                item['country'] = self.country
                address = article.css('.property-row-content').css('.property-row-content-inner').css('.property-row-main').css('.property-row-location a')
                item['address'] = ' / '.join([link.css('::text').get() for link in address])
                item['city'] = article.css('.property-row-content').css('.property-row-content-inner').css('.property-row-main').css('.property-row-location').css('a::text').get()
                price = article.css('.property-row-content').css('.property-row-content-inner').css('.property-row-meta').css('a').css('.property-row-meta-item-price').css('strong::text').get()
                item['price'] = int(re.sub(r'\D', '', price))
                surface = article.css('.property-row-content').css('.property-row-content-inner').css('.property-row-meta').css('a').css('.property-row-meta-item-area').css('strong::text').get()
                item['surface'] = int(re.sub(r'[\D,m2]', '', surface))
                rooms = article.css('.property-row-content').css('.property-row-content-inner').css('.property-row-meta').css('a').css('.property-row-meta-item-beds').css('strong::text').get()
                item['rooms'] = int(re.sub(r'\D', '', rooms))
                item['website'] = self.name
                item['scraped_at'] = self.scraped_at
                yield scrapy.Request(item['url'], meta={'item': item}, callback=self.parse_inner)
            except Exception as e:
                self.handle_exception()

        next_page = response.css('a.next.page-numbers').css('::attr(href)').get()
        if next_page is not None:
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_inner(self, response):
        if self.crawler.stats.get_value('items_scrapeable', 0) >= 30:
            raise CloseSpider(reason="finished")
    
        try:
            item = response.meta['item']
            content = response.css('.content')
            entry_content_div = content[0].css('article').css('.entry-content')
            entry_content_div.css('div.map-position, div.similar-properties').remove()
            description = ''.join(entry_content_div.css('*::text').getall()).replace('\n','').strip()
            description = re.sub(r'\s{2,}', ' ', description)
            item = self.read_description(item, description)
            self.crawler.stats.inc_value('items_scrapeable')
            yield item

        except Exception as e:
            print(str(e))
            self.handle_exception()