import re
import datetime as dt
import logging
import sys

import scrapy

logger = logging.getLogger(__name__)
no_image_url = '"https://stekkies.com/static/roomraider/img/no-image-found.jpg"'
negative_adjectives = ['geen', 'niet', 'ongeschikt', 'onbeschikbaar', 'not', 'no ', 'disallowed', 'verboden', 'nee']
positive_adjectives = ['wel', 'geschikt', 'toegestaan', 'beschikbaar', 'allowed', 'overleg', 'welkom']


class BaseStekkiesSpider(scrapy.Spider):
    def __init__(self, recent_houses_urls, headers, scraper_run, url, country, cities=None, scrape_other_cities=True,
                 listing_type='house'):
        self.balcony = ['balcony', 'balconies', 'balkon', 'balkonnen', 'balkons', 'balkonnetje', 'dakterras',
                        'roofterrace', 'rooftopterrace', 'rooftop terrace']
        self.garden = ['garden', ' tuin', ' tuintje', 'patio']
        self.bath = [' bath', ' bad', 'ligbad', 'jacuzzi', 'bubbelbad']
        self.furnished = [r'\bgemeubileerd\b', r'\bfurnished\b', r'\bgemeubileerde\b']
        self.unfurnished = [r'\bongemeubileerd\b', r'\bunfurnished\b', r'\bongemeubileerde\b']
        self.sharing = ['delers', 'sharing', 'woningdelers', 'delen', 'woningdelen']
        self.pets = ['huisdieren', 'huisdier', r'\bpet\b', r'\bpets\b', 'dieren', 'honden', 'katten']
        self.senior_home = ["senioren", "seniorenwoning", "leeftijdsgrens", "55 jaar", "55\+", "50\+", "ouderenwoning",
                            "aanleunwoning"]
        self.income_requirement = ['inkomen', 'inkomsten', 'verdienen', 'salaris', 'income', 'salary', 'earning']
        self.students = ['student', 'studenten', 'scholier', 'students', 'scholieren', 'studentes', 'studentenkamer',
                         'studentenverhuur']
        self.rotate_user_agent = True
        self.scraped_at = dt.datetime.now().replace(second=0, microsecond=0)
        self.exceptions_count = 0
        self.recent_houses_urls = recent_houses_urls
        self.headers = headers
        self.scraper_run = scraper_run
        self.url = url
        self.cities = cities
        self.country = country
        self.scrape_other_cities = scrape_other_cities
        self.listing_type = listing_type
        super().__init__()

    def check_for_attribute_allowance(self,
                                      description,
                                      keywords,
                                      negative_adjectives=negative_adjectives,
                                      positive_adjectives=positive_adjectives,
                                      ):

        negative_keyword_regex = f'({"|".join(negative_adjectives)})(?:[\w\s,\/]{{,6}}){{0,6}}({"|".join(keywords)})'
        negative_keyword_flipped_regex = f'({"|".join(keywords)})(?:[\w\s,\/]{{,6}}){{0,6}}({"|".join(negative_adjectives)})'

        positive_keyword_regex = f'({"|".join(positive_adjectives)})(?:[\w\s,\/]{{,6}}){{0,6}}({"|".join(keywords)})'
        positive_keyword_flipped_regex = f'({"|".join(keywords)})(?:[\w\s,\/]{{,6}}){{0,6}}({"|".join(positive_adjectives)})'

        if re.search(negative_keyword_regex, description.lower()) or re.search(negative_keyword_flipped_regex,
                                                                               description.lower()):
            return False
        elif re.search(positive_keyword_regex, description.lower()) or re.search(positive_keyword_flipped_regex,
                                                                                 description.lower()):
            return True
        else:
            return None

    def check_for_keywords(self, description, keywords):
        if any(re.search(keyword, description.lower()) for keyword in keywords):
            return True
        else:
            return False

    def read_description(self, item, description_text):
        # Balcony
        item['balcony'] = item['balcony'] if item.get('balcony', None) is not None else self.check_for_keywords(description_text, self.balcony)
        # Garden
        item['garden'] = item['garden'] if item.get('garden', None) is not None else self.check_for_keywords(description_text, self.garden)
        # Bath
        item['bath'] = self.check_for_keywords(description_text, self.bath)
        # Furnished
        item['furnished'] = item['furnished'] if item.get('furnished', None) is not None else self.check_for_keywords(description_text, self.furnished)
        if item['furnished'] == False:
            unfurnished = self.check_for_keywords(description_text, self.unfurnished)
            if unfurnished == False:
                item['furnished'] = None
        # Sharing
        item['sharing'] = self.check_for_attribute_allowance(description_text, self.sharing)
        # Pets
        item['pet_friendly'] = self.check_for_attribute_allowance(description_text, self.pets)
        # Income Requirement
        item['income_requirement'] = self.check_for_keywords(description_text, self.income_requirement)
        # Students
        item['students'] = self.check_for_attribute_allowance(description_text, self.students)
        # Senior Home
        item['senior_home'] = self.check_for_keywords(description_text, self.senior_home)
        # Description
        item['description'] = description_text
        return item

    def handle_exception(self):
        exception_type, exception_object, exception_traceback = sys.exc_info()
        self.crawler.stats.inc_value(
            f'scraper_exceptions/types/{exception_type.__name__}/line/{exception_traceback.tb_lineno}')
        self.crawler.stats.inc_value('scraper_exceptions/errors')
