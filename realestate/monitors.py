from spidermon import Monitor, MonitorSuite, monitors
from spidermon.contrib.monitors.mixins import StatsMonitorMixin
from spidermon.contrib.scrapy.monitors import (UnwantedHTTPCodesMonitor,
                                               FinishReasonMonitor)

@monitors.name('Amount Listings Scrapeable')
class AmountListingsScrapeableMonitor(Monitor, StatsMonitorMixin):
    @monitors.name('There are no items scrapeable')
    def test_minimum_amount_of_listings_scrapeable(self):
        items_scrapeable = getattr(self.stats, 'items_scrapeable', 0)
        listings_present_on_page = getattr(self.stats, 'listings_present_on_page', True)

        if listings_present_on_page:
            self.assertFalse(
                items_scrapeable == 0,
                msg="There are no scrapeable items for this spider"
            )

@monitors.name('Item Validation')
class ItemValidationMonitor(Monitor, StatsMonitorMixin):
    @monitors.name('All items contained validation errors')
    def test_item_validations(self):
        items_validated = getattr(self.data.stats, 'spidermon/validation/items', 0)
        item_validated_with_errors = getattr(self.data.stats, 'spidermon/validation/items/errors', 0)

        if items_validated > 0:
            self.assertFalse(
                items_validated == item_validated_with_errors,
                msg=f"All of the {items_validated} items contained errors"
            )

@monitors.name('Spider Exceptions')
class SpiderExceptionsMonitor(Monitor, StatsMonitorMixin):
    @monitors.name('All items scrapeable contain a spider exception')
    def test_spider_exceptions(self):
        spider_exceptions = getattr(self.data.stats, 'scraper_exceptions/errors', 0)
        items_scrapeable = getattr(self.stats, 'items_scrapeable', 0)
        if spider_exceptions:
            self.assertFalse(
                spider_exceptions == items_scrapeable,
                msg=f"All items scrapeable contain a spider exception"
            )

class SpiderCloseMonitorSuite(MonitorSuite):

    monitors = [
        # Custom Monitors (voeg error count toe + try: except)
        AmountListingsScrapeableMonitor,
        ItemValidationMonitor,
        SpiderExceptionsMonitor,

        # Basic Monitors
        UnwantedHTTPCodesMonitor,
        FinishReasonMonitor
    ]

    monitors_finished_actions = [
        # actions to execute when suite finishes its execution
    ]

    monitors_failed_actions = [
        # SendSlackMessageSpiderFinished
    ]